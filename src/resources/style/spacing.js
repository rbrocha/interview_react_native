import {scaleSize} from './mixins';

export const SCALE_1 = scaleSize(1);
export const SCALE_5 = scaleSize(5);
export const SCALE_12 = scaleSize(12);
export const SCALE_15 = scaleSize(15);
export const SCALE_20 = scaleSize(20);
export const SCALE_48 = scaleSize(48);
export const SCALE_68 = scaleSize(68);
