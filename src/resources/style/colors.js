export const PRIMARY = '#3997F5';
export const SECONDARY = '#656B71';
export const WHITE = '#FFFFFF';
export const BLACK = '#000000';

// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const ALERT = '#FD7080';

// GRAYSCALE
export const GRAY_LIGHT = '#9AA7B5';
export const GRAY_MEDIUM = '#656b7133';
export const GRAY_DARK = '#00000029';
