import styled from 'styled-components';
import {Colors, Typography, Spacing} from '../../../resources/style';

export const Container = styled.SafeAreaView`
  flex: 1;

  align-items: center;
  justify-content: center;

  background: #ffffff;
`;

export const Title = styled.Text`
  color: ${Colors.PRIMARY};
  font-weight: bold;
  font-size: ${Typography.FONT_SIZE_52};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
  margin-bottom: ${Spacing.SCALE_15};
`;

export const SubTitle = styled.Text`
  color: ${Colors.SECONDARY};
  font-size: ${Typography.FONT_SIZE_16};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
  margin-bottom: ${Spacing.SCALE_20};
`;

/******SHARED*******/
export const LinksContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;

  margin-top: ${Spacing.SCALE_5};
  margin-bottom: ${Spacing.SCALE_20};
`;

export const Row = styled.View`
  height: 100%;
  flex-direction: row;
  align-items: center;
`;

export const Bull = styled.Text`
  color: ${Colors.ALERT};
`;

export const LinkLabel = styled.Text`
  color: ${Colors.SECONDARY};
  font-size: ${Typography.FONT_SIZE_14};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
`;

export const FormRequired = styled.Text`
  color: ${Colors.GRAY_LIGHT};
  font-size: ${Typography.FONT_SIZE_14};
  font-family: ${Typography.FONT_FAMILY_REGULAR};
  margin-left: 5px;
`;
