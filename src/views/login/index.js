import React, {useRef, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Form from '../../components/shared/form';
import Input from '../../components/shared/input';
import Button from '../../components/shared/button';
import AuthTemplate from '../../components/templates/auth';

import {I18n} from '../../utils';
import {doSchema, doSubmit, handleIncorrectPwd} from './controller';

const Login = () => {
  const formRef = useRef(null);
  const lanCode = useSelector((state) => state.language.code);
  const signInFail = useSelector((state) => state.auth.signInFail);

  const Dictionary = I18n[lanCode];
  const dispatch = useDispatch();

  const schema = doSchema({Dictionary});

  const handleSubmit = () => {
    doSubmit({formRef, dispatch});
  };

  useEffect(() => {
    handleIncorrectPwd({signInFail, Dictionary, formRef});
  }, [signInFail]);

  return (
    <AuthTemplate
      title={Dictionary.signIn}
      subtitle={Dictionary.enterYourDetails}>
      <Form
        propRef={formRef}
        submitAction={handleSubmit}
        schema={schema}
        style={{width: '70%'}}>
        <Input
          name="email"
          type="text"
          maxLength={254}
          placeholder={Dictionary.email}
          label={Dictionary.email}
          autoCapitalize="none"
        />
        <Input
          name="password"
          secureTextEntry={true}
          maxLength={254}
          placeholder={Dictionary.password}
          label={Dictionary.password}
        />

        <Button formRef={formRef} type="submit">
          {Dictionary.signIn.toUpperCase()}
        </Button>
      </Form>
    </AuthTemplate>
  );
};

export default Login;
