export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: {email, password},
  };
}

export function signInSuccess(token) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: {token},
  };
}

export function signFailure() {
  return {
    type: '@auth/SIGN_FAILURE',
  };
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}

export function recoverRequest(email) {
  return {
    type: '@auth/RECOVER_REQUEST',
    payload: {email},
  };
}

export function recoverSuccess() {
  return {
    type: '@auth/RECOVER_SUCCESS',
  };
}
