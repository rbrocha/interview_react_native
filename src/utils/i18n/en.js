const en = {
  email: 'E-mail',
  password: 'Password',
  signIn: 'Sign In',
  recover: 'Recover',
  recoverPassword: 'Recover Password',
  forgotPass: 'Forgot Password',
  backToSignIn: 'Back to sign in',
  enterYourDetails: 'Enter your detail bellow',
  requirements: 'Requirements',
  error: {
    isRequired: 'is required',
    isInvalid: 'is invalid',
    loginOrPassword: 'E-mail or password incorrect',
  },
};

export default en;
