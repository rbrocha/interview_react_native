# Prime Solutions Interview

##Overview

This test is in React Native and the main goal is to verify your skills in this technology.

Below you can view all information about the test. 

`If you have any questions, you can send an email to rlesteves@primeit.pt`

### Test Time

Starts at 09:00 a.m until 06:00 p.m. 

`We will send an email with the instructions and when you will start the test.`

### Repository

First you need to create a clone repository to start the test. To configure the project follow the instructions in the file README.md

`git clone https://rlesteves@bitbucket.org/rbrocha/interview_react_native.git`

### How to send the test?
Create a zip file with the entire code and send it to the email rlesteves@primeit.pt. Your email subject must be “React Native Test - [Full Name]”.

`You must send your test before 06:00 p.m. If you send it after 06:00 p.m, the test will be invalidated.`


### Project Test
There are some functional requirements (User Stories) that need to be developed.

Login User

List Users

Fix Bug

`All back-end APIs will be provided.`

## Running this app

Before running the app, make sure you ran:

    git clone https://rbrocha@bitbucket.org/rbrocha/interview_react_native.git
    cd interview_react_native
    yarn install

### Running on iOS

macOS, Xcode and CocoaPods are required.

- Install CocoaPods dependencies: `cd ios && pod install`
- Back to root folder and : `react-native run-ios`

### Running on Android

Android SDK, android emulator or android device are required.

Start an Android emulator.

    cd interview_react_native
    reat-native run-android

_Note: Building for the first time can take a while._

If you want to use a physical device, run `adb devices`, then `adb -s <device name> reverse tcp:8081 tcp:8081`.

## Opening ReactoTron

Whith reactotron installed run `adb devices`, then `adb -s <device name> reverse tcp:9090 tcp:9090`
Reload the application.
